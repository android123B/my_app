package com.example.my_app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.TimeZone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;


public class MainActivity1 extends AppCompatActivity {

    private Button options;
    private android.support.v7.widget.Toolbar toolbar;
    private TextView cours1;
    private TextView cours2;
    private ImageView image1;
    private ImageView image2;
    private TextView prochainCours;

    public String str;
    public String str_cours;
    public StringBuilder azop = new StringBuilder();
    public  StringBuilder azop2 = new StringBuilder();

    public StringBuilder azopEval = new StringBuilder();
    public  StringBuilder azopEval2 = new StringBuilder();

    public StringBuilder azopSalle = new StringBuilder();
    //public ArrayList azopSalle = new ArrayList();

    public StringBuilder CalDay = new StringBuilder();
    public  StringBuilder CalDay2 = new StringBuilder();
    private java.util.Calendar mCurrentDate;

    public static SharedPreferences sharedPreferences;
    public static String SEL_Group;
    private static final String PREFS = "PREFS";
    private String space = "      ";
    public SwipeRefreshLayout swipe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        sharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        if(!sharedPreferences.contains(SEL_Group)){
            sharedPreferences.edit().putString(SEL_Group, "L3INFO_TD2").apply();
        }
        setupUIViews();
        initToolbar();
        setupView();
        if (!isConnected()) {
            return;
        }
        //new FetchTask().execute("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
        new FetchTask().execute("https://accueil-ent2.univ-avignon.fr/edt/exportAgendaUrl?codeDip=2-L3IN");

    }

    private void setupUIViews(){
        toolbar = (Toolbar)findViewById(R.id.Toolbar1);
        options = (Button)findViewById(R.id.button2);
        cours1 = (TextView)findViewById(R.id.textView5);
        cours2 = (TextView)findViewById(R.id.textView6);
        image1 = (ImageView)findViewById(R.id.imageView4);
        image2 = (ImageView)findViewById(R.id.imageView5);
        prochainCours = (TextView)findViewById(R.id.textTime);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ceri App"+ space +sharedPreferences.getString(SEL_Group,null));
    }

    private void setupView(){
        options.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intento = new Intent(MainActivity1.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("aaa",azop.toString());
                bundle.putString("bbb",azop2.toString());
                intento.putExtra("bundle",bundle);

                Bundle bundle1 = new Bundle();
                bundle1.putString("aaa",azopEval.toString());
                bundle1.putString("bbb",azopEval2.toString());
                intento.putExtra("bundle1",bundle1);

                Bundle bundle2 = new Bundle();
                if(azopSalle.toString().isEmpty()){
                    azopSalle.append("vide");
                }
                bundle2.putString("aaa",azopSalle.toString());
                intento.putExtra("bundle2",bundle2);

                startActivity(intento);
            }
        });
    }


    private void MajListView(Date date1,String ajout,Date date2, String ajout2,int etat,String prochainCours)
    {
        String tab[] = ajout.split(" - ue ");
        String tab2[] = ajout2.split(" - ue ");
        cours1.setText(tab[1]);
        cours2.setText(tab2[1]);
        this.prochainCours.setText(prochainCours);


        Date actuelle = new Date();

        if(etat ==1){
            image1.setImageResource(R.drawable.dejacommence);
        }

    }

    public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit)
    {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    public Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }



    private boolean isConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private class FetchTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(MainActivity1.this);
            dialog.setMessage("Doing something, please wait...");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            InputStream inputStream = null;
            HttpURLConnection conn = null;

            String stringUrl = strings[0];
            try {
                URL url = new URL(stringUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int response = conn.getResponseCode();
                if (response != 200) {
                    return null;
                }

                inputStream = conn.getInputStream();
                if (inputStream == null) {
                    return null;
                }

                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader reader = new BufferedReader(inputStreamReader);
                StringBuffer buffer = new StringBuffer();
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    buffer.append("\n");
                }

                return new String(buffer);
            } catch (IOException e) {
                return null;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            if (s == null) {
                str = "erreur";
                MajListView(null,str,null,str,0,str);
            } else {
                func(s);
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }


        private void func(String s){

            try {

                StringReader sin = new StringReader(s);

                CalendarBuilder builder = new CalendarBuilder();

                Calendar calendar = builder.build(sin);

                StringBuilder az = new StringBuilder();
                String tab[];

                HashMap<Date, String> hashmap = new HashMap<Date, String>();
                HashMap<Date, String> hashmapDay = new HashMap<Date, String>();
                HashMap<Date, String> hashmapEval = new HashMap<Date, String>();

               // Date actuelle = parseDate("2019-05-02-08-45-00"); // pour simuler la date
                Date actuelle = new Date();

                for (Iterator i = calendar.getComponents().iterator(); i.hasNext();)
                {
                    Component component = (Component) i.next();

                    String icalvalue = component.getProperty("DTSTART").getValue();
                    String icalvalue2 = component.getProperty("DTEND").getValue();
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyyMMdd'T'HHmmss'Z'");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


                    DateFormat dateFormato = new SimpleDateFormat("dd MM yyyy");
                    String dat = dateFormato.format(actuelle);
                    String der = "";
                    String der2 = "";

                    Date date = null;
                    Date date2 = null;
                    try {
                        date = dateFormat.parse(icalvalue);
                        der = dateFormato.format(date);

                        date2 = dateFormat.parse(icalvalue2);
                        der2 = dateFormato.format(date2);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(date!= null) {
                        long ecart = getDateDiff(date, actuelle, TimeUnit.MINUTES);


                        if (date != null && date.after(actuelle) || (ecart < 16 && ecart > 0)) {
                            if (sharedPreferences.contains(SEL_Group)) {
                                String pref = sharedPreferences.getString(SEL_Group, null);

                                tab = component.getProperty("DESCRIPTION").getValue().split("\n");
                                if (!tab[0].startsWith("Annulation") && tab.length > 2 && (tab[2].contains(pref) || tab[2].startsWith("Promotion"))) {
                                    hashmap.put(date, tab[0].toLowerCase() + "\n" + tab[1] + "\n" + tab[3]);
                                    if (tab[4].contains("Evaluation")) {
                                        hashmapEval.put(date, tab[0].toLowerCase());
                                    }
                                }
                            }

                        }
                    }

                    if(Objects.equals(der, dat)) // si le meme jours
                    {
                        if (sharedPreferences.contains(SEL_Group))
                        {
                            String pref = sharedPreferences.getString(SEL_Group, null);
                            tab = component.getProperty("DESCRIPTION").getValue().split("\n");
                            if (!tab[0].startsWith("Annulation") && tab.length > 2 && (tab[2].contains(pref) || tab[2].startsWith("Promotion")))
                            {
                                hashmapDay.put(date,tab[0].toLowerCase());
                            }
                        }

                        if(date != null && date2 != null && date.equals(actuelle) || (date.before(actuelle) && date2.after(actuelle)))
                        {
                            tab = component.getProperty("DESCRIPTION").getValue().split("\n");
                            tab = tab[3].split(":");

                            azopSalle.append(tab[1]+"\n");

                        }
                    }



                }


                if(hashmapDay.isEmpty()){
                    azop.append("vide");
                    azop2.append("vide");
                }else{
                    Map<Date, String> mapDay = new TreeMap<Date, String>(hashmapDay);
                    Set setDay = mapDay.entrySet();
                    Iterator itDay = setDay.iterator();
                    while (itDay.hasNext()){
                        Map.Entry entryDay = (Map.Entry)itDay.next();
                        azop.append(entryDay.getValue().toString()+"\n");
                        azop2.append(entryDay.getKey().toString()+"\n");
                    }

                }

                Map<Date, String> mapEval = new TreeMap<Date, String>(hashmapEval);
                Set setEval = mapEval.entrySet();
                Iterator itEval = setEval.iterator();
                while (itEval.hasNext()){
                    Map.Entry entryEval = (Map.Entry)itEval.next();
                    azopEval.append(entryEval.getValue().toString()+"\n");
                    azopEval2.append(entryEval.getKey().toString()+"\n");
                }


                Map<Date, String> map = new TreeMap<Date, String>(hashmap);
                Set set = map.entrySet();
                Iterator it = set.iterator();
                Map.Entry entry = (Map.Entry)it.next(); // on prend le premier

                java.util.Date mydate = (java.util.Date) entry.getKey();  // prend la date
                String text = "";
                int etat = 0;


                Long diff = getDateDiff(mydate,actuelle,TimeUnit.MINUTES); // < 15 et > 0

                if(diff < 16)
                {
                    text = entry.getValue().toString()+"\n"+entry.getKey().toString();  //prochain cours
                    if(diff >0){
                        etat = 1;
                    }
                }else{ // on prend le suivant
                    entry = (Map.Entry)it.next();
                    mydate = (java.util.Date) entry.getKey();
                    text = entry.getValue().toString()+"\n"+entry.getKey().toString();  //prochain cours
                }

                entry = (Map.Entry)it.next();

                String text2 = entry.getValue().toString()+"\n"+entry.getKey().toString();   // 2 eme cours
                java.util.Date mydate2 = (java.util.Date) entry.getKey();

                Long nombreHeure = (diff / 60)*-1;
                diff %=60;
                diff*=-1;

                String prochainCours = "Prochain cours dans : "+nombreHeure.toString()+"h "+diff.toString()+" minutes";

                MajListView(mydate,text,mydate2,text2,etat,prochainCours);



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserException e) {
                e.printStackTrace();
            }
        }
    }


}