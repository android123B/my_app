package com.example.my_app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my_app.Utils.LetterImageView;

import java.util.Objects;

public class DayDetail extends AppCompatActivity {

    private ListView listView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_detail);

        setupUIViews();
        initToolbar();
        setupListView();
    }

    private void setupUIViews(){
        listView = (ListView)findViewById(R.id.lvDayDetail);
        toolbar = (Toolbar)findViewById(R.id.ToolbarDayDetail);
    }


    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setupListView(){

        Intent intent = getIntent();

        Bundle bundle = intent.getBundleExtra("bundle");

        if(Objects.equals(bundle.getString("aaa"), "vide"))
        {
            String goodDay = "THIS IS A GOOD DAY \n";
            String gooddayTime = "Pas cours de la journee !! \n";
            String good[] = goodDay.split("\n");
            String good2[] = gooddayTime.split("\n");

            SimpleAdapter simpleAdapter = new SimpleAdapter(this,good,good2);
            listView.setAdapter(simpleAdapter);

        }else
        {
            String[] buff1 = bundle.getString("aaa").split("\n");
            String[] buff2 = bundle.getString("bbb").split("\n");

            SimpleAdapter simpleAdapter = new SimpleAdapter(this,buff1,buff2);
            listView.setAdapter(simpleAdapter);
        }
    }

    public class SimpleAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater layoutInflater;
        private TextView subject,time;
        private String[] subjectArray;
        private String[] timeArray;
        private LetterImageView letterImageView;

        public SimpleAdapter(Context context,String[] subjectArray,String[] timeArray){
            mContext = context;
            this.subjectArray = subjectArray;
            this.timeArray = timeArray;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return subjectArray.length;
        }

        @Override
        public Object getItem(int position) {
            return subjectArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.day_detail_single_item,null);
            }

            subject = (TextView)convertView.findViewById(R.id.tvSubjectDayDetails);
            time = (TextView)convertView.findViewById(R.id.tvTimeDayDetail);
            letterImageView = (LetterImageView)convertView.findViewById(R.id.ivDayDetails);

            subject.setText(subjectArray[position]);
            time.setText(timeArray[position]);

            letterImageView.setOval(true);
            letterImageView.setLetter(subjectArray[position].charAt(13));

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home : {
                onBackPressed();
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

}
