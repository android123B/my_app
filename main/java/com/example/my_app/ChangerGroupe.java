package com.example.my_app;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.my_app.Utils.LetterImageView;

public class ChangerGroupe extends AppCompatActivity
{

    private Toolbar toolbar;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupe);

        setupUIViews();
        initToolbar();
        setupListView();
    }

    private void setupUIViews(){
        toolbar = (Toolbar)findViewById(R.id.ToolbarGroupe);
        listView = (ListView)findViewById(R.id.lvGroupe);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Changer Groupe");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setupListView(){
        String[] groupe = getResources().getStringArray(R.array.groupe);

        GroupeAdapter adapter = new GroupeAdapter(this,R.layout.activity_groupe_single_item,groupe);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD1").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    case 1: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD2").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    case 2: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD3").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    case 3: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD4").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    case 4: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD5").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    case 5: {
                        MainActivity.sharedPreferences.edit().putString(MainActivity.SEL_Group, "L3INFO_TD6").apply();
                        startActivity(new Intent(ChangerGroupe.this, MainActivity1.class));
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        });
    }

    public class GroupeAdapter extends ArrayAdapter {

        private int resource;
        private LayoutInflater layoutInflater;
        private String[] groupe = new String[]{};

        public GroupeAdapter(Context context, int resource,String[] objects)
        {
            super(context, resource,objects);
            this.resource = resource;
            this.groupe = objects;
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
            ViewHolder holder;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = layoutInflater.inflate(resource,null);
                holder.ivLogo = (LetterImageView)convertView.findViewById(R.id.ivGroupe);
                holder.tvGroupe = (TextView)convertView.findViewById(R.id.tvGroupe);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }

            holder.ivLogo.setOval(true);
            holder.ivLogo.setLetter('G');
            holder.tvGroupe.setText(groupe[position]);

            return convertView;
        }

        class ViewHolder{
            private LetterImageView ivLogo;
            private TextView tvGroupe;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home : {
                onBackPressed();
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
