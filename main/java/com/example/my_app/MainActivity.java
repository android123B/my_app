package com.example.my_app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.TimeZone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;


public class MainActivity extends AppCompatActivity {

    private android.support.v7.widget.Toolbar toolbar;
    private ListView listView;

    private java.util.Calendar mCurrentDate;

    public static SharedPreferences sharedPreferences;
    public static String SEL_Group;
    private static final String PREFS = "PREFS";
    private String space = "      ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        if(!sharedPreferences.contains(SEL_Group)){
            sharedPreferences.edit().putString(SEL_Group, "L3INFO_TD2").apply();
        }
        setupUIViews();
        initToolbar();
        setupListView();
    }

    private void setupUIViews(){
        toolbar = (Toolbar)findViewById(R.id.ToolbarMain);
        listView = (ListView)findViewById(R.id.lvMain);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ceri App"+ space +sharedPreferences.getString(SEL_Group,null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setupListView(){

        String[] title = getResources().getStringArray(R.array.Main);
        String[] description = getResources().getStringArray(R.array.Description);

        SimpleAdapter simpleAdapter = new SimpleAdapter(this,title,description);

        listView.setAdapter(simpleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0: {
                        Intent intent = getIntent();
                        Bundle bundle = intent.getBundleExtra("bundle");
                        Intent intento = new Intent(MainActivity.this, DayDetail.class);
                        intento.putExtra("bundle",bundle);
                        startActivity(intento);
                        break;
                    }
                    case 1: {
                        Intent intent = getIntent();
                        Bundle bundle = intent.getBundleExtra("bundle2");
                        Intent intento = new Intent(MainActivity.this, WeekActivity.class);
                        intento.putExtra("bundle",bundle);
                        startActivity(intento);
                        break;
                    }
                    case 2: {
                        mCurrentDate = java.util.Calendar.getInstance();
                        int year = mCurrentDate.get(java.util.Calendar.YEAR);
                        int month = mCurrentDate.get(java.util.Calendar.MONTH);
                        int day = mCurrentDate.get(java.util.Calendar.DAY_OF_MONTH);

                        DatePickerDialog mDatePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Selected_year, int Selected_month, int Selected_dayOfMonth)
                            {
                                String ladate = String.format("%02d",Selected_dayOfMonth)+" "+String.format("%02d",Selected_month+1)+" "+String.format("%02d", Selected_year);


                                Intent intent = new Intent(MainActivity.this,DayCalendar.class);
                                intent.putExtra("calday",ladate);
                                startActivity(intent);
                            }
                        }, year, month, day);


                        mDatePicker.show();
                        break;
                    }
                    case 3: {
                        Intent intent = getIntent();
                        Bundle bundle = intent.getBundleExtra("bundle1");
                        Intent intenti = new Intent(MainActivity.this,DayDetail.class);
                        intenti.putExtra("bundle",bundle);
                        startActivity(intenti);
                        break;
                    }
                    case 4: {
                        Intent intenta = new Intent(MainActivity.this,ChangerGroupe.class);
                        startActivity(intenta);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        });

    }

 /*   private void MajListView(String ajout, String ajout2)
    {
        String tab[] = ajout.split("\n");
        String tab2[] = ajout2.split("\n");

        String[] title = getResources().getStringArray(R.array.Main);
        String[] description = getResources().getStringArray(R.array.Description);

        ArrayList list = new ArrayList(Arrays.asList(title));
        list.add(0,tab[0]);
        list.add(1,tab2[0]);
        ArrayList list2 = new ArrayList(Arrays.asList(description));
        list2.add(0,tab[1]);
        list2.add(1,tab2[1]);

        title = (String[])list.toArray(new String[list.size()]);
        description = (String[])list2.toArray(new String[list2.size()]);

        SimpleAdapter simpleAdapter = new SimpleAdapter(this,title,description);

        listView.setAdapter(simpleAdapter);

    }*/


    public class SimpleAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater layoutInflater;
        private TextView title,description;
        private String[] titleArray;
        private String[] descriptionArray;
        private ImageView imageView;

        public SimpleAdapter(Context context,String[] title,String[] description){
            mContext = context;
            titleArray = title;
            descriptionArray = description;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return titleArray.length;
        }

        @Override
        public Object getItem(int position) {
            return titleArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.main_activity_single_item,null);
            }

            title = (TextView)convertView.findViewById(R.id.tvMain);
            description = (TextView)convertView.findViewById(R.id.tvDescription);
            imageView = (ImageView)convertView.findViewById(R.id.ivMain);

            title.setText(titleArray[position]);
            description.setText(descriptionArray[position]);

            if(titleArray[position].equalsIgnoreCase("Salle")){
                imageView.setImageResource(R.drawable.loupe);
            }else if(titleArray[position].equalsIgnoreCase("Options")){
                imageView.setImageResource(R.drawable.settings);
            }else if(titleArray[position].equalsIgnoreCase("Calendar")) {
                imageView.setImageResource(R.drawable.calendar);
            }else if(titleArray[position].equalsIgnoreCase("Day")){
                imageView.setImageResource(R.drawable.today);
            } else {
                imageView.setImageResource(R.drawable.prochain);
            }

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home : {
                onBackPressed();
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }


}
