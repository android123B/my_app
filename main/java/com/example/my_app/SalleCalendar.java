package com.example.my_app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.my_app.Utils.LetterImageView;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.TimeZone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

public class SalleCalendar extends AppCompatActivity {

    private ListView listView;
    private Toolbar toolbar;

    public String str;
    public StringBuilder azopSalle = new StringBuilder();

    public StringBuilder buff = new StringBuilder();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salle_calendar);

        setupUIViews();
        initToolbar();


        if (!isConnected()) {
            return;
        }
        new SalleCalendar.FetchTask().execute("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
    }

    private void setupUIViews(){
        listView = (ListView)findViewById(R.id.lvDayDetail);
        toolbar = (Toolbar)findViewById(R.id.ToolbarDayDetail);
    }



    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Salles Occupés");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private boolean estPresent(String element, String[] tab) {

        for (int i = 0; i < tab.length; i++) {
            if(element.startsWith(tab[i])){
                return true;
            }
        }
        return false;
    }



    private void setupListView(StringBuilder azopSalle){


        if(Objects.equals(azopSalle.toString(), "vide"))
        {
            String goodDay = "TOUTES LES SALLES SONT LIBRE !!! \n";

            String good[] = goodDay.split("\n");


            SalleCalendar.SimpleAdapter simpleAdapter = new SalleCalendar.SimpleAdapter(this,good);
            listView.setAdapter(simpleAdapter);

        }else
        {
            String[] buff1 = azopSalle.toString().split("\n");

            for (int i = 0; i < buff1.length; i++) {
                if(buff1[i].contains("S") || buff1[i].contains("Amphi")){
                    buff.append(buff1[i].toString()).append("\n");
                }

            }

            String[] buf = buff.toString().split("\n");

            SalleCalendar.SimpleAdapter simpleAdapter = new SalleCalendar.SimpleAdapter(this,buf);
            listView.setAdapter(simpleAdapter);
        }
    }

    private boolean isConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    private class FetchTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(SalleCalendar.this);
            dialog.setMessage("Doing something, please wait.");
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            InputStream inputStream = null;
            HttpURLConnection conn = null;

            String stringUrl = strings[0];
            try {
                URL url = new URL(stringUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int response = conn.getResponseCode();
                if (response != 200) {
                    return null;
                }

                inputStream = conn.getInputStream();
                if (inputStream == null) {
                    return null;
                }

                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader reader = new BufferedReader(inputStreamReader);
                StringBuffer buffer = new StringBuffer();
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    buffer.append("\n");
                }

                return new String(buffer);
            } catch (IOException e) {
                return null;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            if (s == null) {
                str = "erreur";

            } else {
                func(s);
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }


        private void func(String s){

            try {

                StringReader sin = new StringReader(s);

                CalendarBuilder builder = new CalendarBuilder();

                Calendar calendar = builder.build(sin);

                String tab[];



                Intent intent = getIntent();
                String actuelle = intent.getStringExtra("Sallelday");

                Log.d("teste",actuelle);

                Date dato = parseDate(actuelle);

                Log.d("verif",dato.toString());

                for (Iterator i = calendar.getComponents().iterator(); i.hasNext();) {
                    Component component = (Component) i.next();

                    String icalvalue = component.getProperty("DTSTART").getValue();
                    String icalvalue2 = component.getProperty("DTEND").getValue();
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyyMMdd'T'HHmmss'Z'");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


                    DateFormat dateFormato = new SimpleDateFormat("dd MM yyyy");
                    String dat = dateFormato.format(dato);
                    String der = "";

                    Date date = null;
                    Date date2 = null;
                    try {

                        date = dateFormat.parse(icalvalue);
                        der = dateFormato.format(date);
                        date2 = dateFormat.parse(icalvalue2);


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    if(Objects.equals(der, dat)) // si le meme jours
                    {

                        if(date != null && date2 != null && date.equals(dato) || (date.before(dato) && date2.after(dato)))
                        {
                            tab = component.getProperty("DESCRIPTION").getValue().split("\n");
                            tab = tab[3].split(":");

                            azopSalle.append(tab[1]+"\n");

                        }
                    }

                }


                if(azopSalle.toString().isEmpty()){
                    azopSalle.append("vide");
                }

                setupListView(azopSalle);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserException e) {
                e.printStackTrace();
            }
        }
    }


    public class SimpleAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater layoutInflater;
        private TextView subject,time;
        private String[] subjectArray;
        private String[] timeArray;
        private LetterImageView letterImageView;

        public SimpleAdapter(Context context,String[] subjectArray){
            mContext = context;
            this.subjectArray = subjectArray;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return subjectArray.length;
        }

        @Override
        public Object getItem(int position) {
            return subjectArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.day_detail_single_item,null);
            }

            subject = (TextView)convertView.findViewById(R.id.tvSubjectDayDetails);
            time = (TextView)convertView.findViewById(R.id.tvTimeDayDetail);
            letterImageView = (LetterImageView)convertView.findViewById(R.id.ivDayDetails);

            subject.setText(subjectArray[position]);
            time.setText("La salle est occupé !");

            letterImageView.setOval(true);
            letterImageView.setLetter('S');

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home : {
                onBackPressed();
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

}
