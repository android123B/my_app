package com.example.my_app;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.my_app.Utils.LetterImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class WeekActivity extends AppCompatActivity {

    private ListView listView;
    private Toolbar toolbar;
    public StringBuilder buff = new StringBuilder();
    private Button button;
    private java.util.Calendar mCurrentDate;
    private java.util.Calendar mcurrentTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salle_detail);

        setupUIViews();
        initToolbar();
        setupListView();
    }

    private void setupUIViews(){
        listView = (ListView)findViewById(R.id.lvDayDetail);
        toolbar = (Toolbar)findViewById(R.id.ToolbarDayDetail);
        button = (Button)findViewById(R.id.button);
    }


    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Salles Occupées");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private void setupListView(){

        Intent intent = getIntent();

        Bundle bundle = intent.getBundleExtra("bundle");

        if(Objects.equals(bundle.getString("aaa"), "vide"))
        {

            String goodDay = "TOUTES LES SALLES SONT LIBRE !!! \n";

            String good[] = goodDay.split("\n");


            WeekActivity.SimpleAdapter simpleAdapter = new WeekActivity.SimpleAdapter(this,good);
            listView.setAdapter(simpleAdapter);

        }else
        {
            String[] buff1 = bundle.getString("aaa").split("\n");

            for (int i = 0; i < buff1.length; i++) {
                if(buff1[i].contains("S") || buff1[i].contains("Amphi")){
                    buff.append(buff1[i].toString()).append("\n");
                }

            }


            String[] buf = buff.toString().split("\n");

            SimpleAdapter simpleAdapter = new SimpleAdapter(this,buf);
            listView.setAdapter(simpleAdapter);
        }

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCurrentDate = java.util.Calendar.getInstance();
                int year = mCurrentDate.get(java.util.Calendar.YEAR);
                int month = mCurrentDate.get(java.util.Calendar.MONTH);
                int day = mCurrentDate.get(java.util.Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(WeekActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int Selected_year, int Selected_month, int Selected_dayOfMonth)
                    {
                        final String ladate = String.format("%02d",Selected_year)+"-"+String.format("%02d",Selected_month+1)+"-"+String.format("%02d", Selected_dayOfMonth);

                        mcurrentTime = java.util.Calendar.getInstance();
                        int hour = mcurrentTime.get(java.util.Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(WeekActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                String heure = selectedHour + "-" + selectedMinute+"-00";

                                String maDate = ladate+"-"+heure;

                                Toast.makeText(getApplicationContext(),maDate.toString(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(WeekActivity.this,SalleCalendar.class);
                                intent.putExtra("Sallelday",maDate.toString());
                                startActivity(intent);
                            }
                        }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");

                        mTimePicker.show();

                    }

                }, year, month, day);



                mDatePicker.show();

            }
        });
    }

    public class SimpleAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater layoutInflater;
        private TextView subject,time;
        private String[] subjectArray;
        private String[] timeArray;
        private LetterImageView letterImageView;

        public SimpleAdapter(Context context,String[] subjectArray){
            mContext = context;
            this.subjectArray = subjectArray;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return subjectArray.length;
        }

        @Override
        public Object getItem(int position) {
            return subjectArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.day_detail_single_item,null);
            }

            subject = (TextView)convertView.findViewById(R.id.tvSubjectDayDetails);
            time = (TextView)convertView.findViewById(R.id.tvTimeDayDetail);
            letterImageView = (LetterImageView)convertView.findViewById(R.id.ivDayDetails);

            subject.setText(subjectArray[position]);
            time.setText("La salle est occupé !");

            letterImageView.setOval(true);
            letterImageView.setLetter('S');

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home : {
                onBackPressed();
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

}
